#!/bin/sh
# Shell program display processes with CPU  and  MEMORY greater than a threashold
echo "Process Management......"
ps aux | sort -nrk 3 | head | awk '{print $2 " " $3 " " $4 " "  $11 }' > psout
cat psout
read -p "Enter Threshold Level for CPU: " th1
read -p " Enter Threshold for Memory: " th2
while IFS= read line
do
pno=$(echo $line | awk '{print $1}')
cpuload=$(echo $line | awk '{print $2}')
memload=$(echo $line | awk '{print $3}')
pname=$(echo $line | awk '{print $4}')
 if [ $(echo "$cpuload >= $th1" | bc -l) -eq 1 ] || [ $(echo "$memload >= $th2" | bc -l) -eq 1 ] 
then 
	echo "$pno $cpuload $memload $pname"
	kill $pno
	echo "Killed $pno $cpuload $memload $pname"
fi
done <"psout"
