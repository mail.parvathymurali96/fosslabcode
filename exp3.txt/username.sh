#! /bin/bash
#argument count
echo "Number of argument $#"
echo "The arguments are:"
echo $1
echo $2
# check for number of argument
if ! [ $# -eq 2 ]
then 	
	echo "Two argument neeeded"
	exit
fi
#file exist or not
if ! [ -f $1 ]
then
	echo "File named $1 not found"
	exit
else
	#printing file on screen
	echo "FOUND!!! The file is "
	cat $1
fi
if grep -qw "$2" "$1"
then
	echo "Name $2 already exist"
else
	echo "Name $2 has to be added"
	echo "$2">>"$1"
	echo "The file is "
	cat $1
fi


