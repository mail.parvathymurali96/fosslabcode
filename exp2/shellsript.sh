#! /bin/bash
echo "Operating Details:"
cat /etc/issu.net
cat /etc/os-release

echo "Kernel Version:"
uname -r

printf "\nAvailable Shells:"
cat /etc/shells

printf "\nMouse Settings:"
xinput | grep "pointer"

echo "CPU Information"
cat /proc/cpuinfo

echo "Memory Information"
cat /proc/meminfo

echo "File Mounted"
cat /proc/mounts

echo "Hard disk Information"
sudo fdisk -l

